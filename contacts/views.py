from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.mail import send_mail
from decimal import Decimal
from .models import Contact

def contact(request):
  if request.method == 'POST':
    title = request.POST['title']
    contactor_id = request.POST['contactor_id']
    contactee_id = request.POST['contactee_id']
    amount = request.POST.get('amount')
    is_pending = request.POST['is_pending']
    message = request.POST['message']

    contact = Contact(title=title, contactor_id=contactor_id, contactee_id=contactee_id, is_pending=is_pending, message=message, amount=amount)

    contact.save()

    messages.success(request, 'Your request has been submitted, a realtor will get back to you soon')
    return redirect('dashboard')

