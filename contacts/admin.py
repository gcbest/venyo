from django.contrib import admin

from .models import Contact

class ContactAdmin(admin.ModelAdmin):
  list_display = ('id', 'title', 'is_pending', 'amount', 'message')
  list_display_links = ('id', 'title')
  search_fields = ('id', 'title')
  list_per_page = 25

admin.site.register(Contact, ContactAdmin)
