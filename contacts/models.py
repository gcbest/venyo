from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


class Contact(models.Model):
  title = models.CharField(max_length=200)
  contactor = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='contactor')
  contactee = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='contactee')
  is_pending = models.BooleanField(default=True)
  amount = models.DecimalField(blank=True, decimal_places=2, default=1, max_digits=10)
  message = models.TextField(blank=True)
  contact_date = models.DateTimeField(default=datetime.now, blank=True)

  @property
  def user(self):
      return User.objects.get(pk=self.contactor_id)



  def __str__(self):
    return self.title