from django.urls import path
from .views import PaymentListView

from . import views

urlpatterns = [
    path('charge/', views.charge, name='charge'),
    path('all', PaymentListView.as_view(), name='payments_all'),
]
