from django.db import models
from datetime import datetime
from django.utils import timezone

from django.contrib.auth.models import User
from contacts.models import Contact

class Payment(models.Model):
  title = models.CharField(max_length=200)
  recipient = models.IntegerField()
  payer = models.IntegerField()
  amount = models.DecimalField(default=1,blank=True, max_digits=10, decimal_places=2)
  message = models.TextField(blank=True)
  payment_date = models.DateTimeField(default=datetime.now, blank=True)
  contact = models.IntegerField(blank=True) #contact id

  @property
  def recipient_user(self):
      return User.objects.get(pk=self.recipient)

  @property
  def payer_user(self):
      return User.objects.get(pk=self.payer)

  def __str__(self):
    return self.title