import stripe

from django.conf import settings
from django.views.generic.base import TemplateView
from django.views.generic import ListView
from django.utils import timezone
from django.shortcuts import render, redirect
from payments.models import Payment
from contacts.models import Contact

from datetime import datetime
from django.contrib import messages


stripe.api_key = settings.STRIPE_SECRET_KEY

class PaymentListView(ListView):
  model = Payment
  template_name = 'payments/all.html'
  context_object_name = 'payments'
  def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


def charge(request): 
  if request.method == 'POST':
      #create new payment
      title = request.POST['title']
      recipient = request.POST['recipient_id']
      payer = request.POST.get('payer_id')
      amount = request.POST.get('amount', 50)
      message = request.POST['message']
      payment_date = datetime.now()
      receive_contact_id = request.POST['receive_contact_id']
      payment_contact_id = request.POST['payment_contact_id']

      listing = Payment(title=title, recipient=recipient, payer=payer, amount=amount, message=message, payment_date=payment_date, contact=receive_contact_id)
      listing.save()

      #update is_pending on contact
      Contact.objects.filter(pk=receive_contact_id).update(is_pending=False)

      messages.success(request, 'Your payment has been submitted')
      charge = stripe.Charge.create(
          amount='',
          currency='usd',
          description='',
          source=request.POST['stripeToken']
      )
      return redirect('dashboard')