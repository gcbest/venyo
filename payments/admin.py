from django.contrib import admin

from .models import Payment

class ListingAdmin(admin.ModelAdmin):
  list_per_page = 25

admin.site.register(Payment, ListingAdmin)
