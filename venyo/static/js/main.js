const date = new Date();
document.querySelector('.year').innerHTML = date.getFullYear();

setTimeout(function() {
  $('#message').fadeOut('slow');
}, 3000);

$(document).on('click', '.show-contact-modal', function() {
  var contactee_id = $(this).data('contactee_id'); // receiving the contact
  var contactee_name = $(this).data('contactee_name');

  $('.modal-body #contactee-id').val(contactee_id);
  $('#contactee-name').html(contactee_name);

  // var contactor_id = $(this).data('contactor_id'); // receiving the contact
  // var contactor_name = $(this).data('contactor_name');

  // $('.modal-body #contactor-id').val(contactor_id);
  // $('#contactor-name').html(contactor_name);
});

$(document).on('click', '.show-repayment-modal', function() {
  var recipient_id = $(this).data('recipient_id');
  var recipient_name = $(this).data('recipient_name');
  var repayment_amount = $(this).data('repayment_amount');
  var receive_contact_id = $(this).data('receive_contact_id');
  // var payment_contact_id = $(this).data('payment_contact_id');

  $('.modal-body #recipient-id').val(recipient_id);
  $('.modal-body #repayment-id').val(repayment_amount);
  $('.modal-body #repayment-amt').val(repayment_amount);
  $('.modal-body #receive-contact-id').val(receive_contact_id);
  $('.modal-body #payment-contact-id').val(payment_contact_id);
  $('#repay-recipient-name').html(recipient_name);
});

$(document).on('click', '.show-payment-modal', function() {
  var payee_id = $(this).data('payee_id');
  var payee_name = $(this).data('payee_name');
  // var payment_amount = $(this).data('payment_amount');
  var receive_contact_id = $(this).data('receive_contact_id');
  var payment_contact_id = $(this).data('payment_contact_id');

  $('.modal-body #payee-id').val(payee_id);
  $('.modal-body #payee-name').val(payee_name);

  // $('.modal-body #recipient_id').val(payyee_id);
  // $('.modal-body #repayment-amt').val(payment_amount);
  $('.modal-body #receive-contact-id').val(receive_contact_id);
  $('.modal-body #payment-contact-id').val(payment_contact_id);
  $('.stripe-button').attr('data-description', 'Placeholder');
  // $('.stripe-button').data('amount', 300);
  $('#payee-name').html(payee_name);

  // $('#stripe-button').click(function(){
  //   var token = function(res){
  //     var $id = $('<input type=hidden name=stripeToken />').val(res.id);
  //     var $email = $('<input type=hidden name=stripeEmail />').val(res.email);
  //     $('form').append($id).append($email).submit();
  //   };

  //   var title = $('#payment-title').val();
  //   var message = $('#payment-message').val();
  //   var amount = $("#payment-amount").val();
  //   StripeCheckout.open({
  //     key:         'pk_test_iMvrlGn1bW8rvmua34g9THC6',
  //     amount:      parseFloat(amount) * 100,
  //     name:        title,
  //     image:       '',
  //     description: message,
  //     panelLabel:  'Send',
  //     token:       token
  //   });

  //   return false;
  // });
});