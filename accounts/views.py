import stripe
from django.shortcuts import render, redirect
from django.contrib import messages, auth
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.generic import ListView
from contacts.models import Contact
from payments.models import Payment
from datetime import datetime
from .forms import UserUpdateForm
from django.views.generic.base import TemplateView
stripe.api_key = settings.STRIPE_SECRET_KEY


class HomePageView(TemplateView):
    template_name = 'accounts/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['key'] = settings.STRIPE_PUBLISHABLE_KEY
        return context

def register(request):
  if request.method == 'POST':
    # Get form values
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']
    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']
    password2 = request.POST['password2']

    # Check if passwords match
    if password == password2:
      # Check username
      if User.objects.filter(username=username).exists():
        messages.error(request, 'That username is taken')
        return redirect('register')
      else:
        if User.objects.filter(email=email).exists():
          messages.error(request, 'That email is being used')
          return redirect('register')
        else:
          # Looks good
          user = User.objects.create_user(username=username, password=password,email=email, first_name=first_name, last_name=last_name)
          # Login after register
          # auth.login(request, user)
          # messages.success(request, 'You are now logged in')
          # return redirect('index')
          user.save()
          messages.success(request, 'You are now registered and can log in')
          return redirect('login')
    else:
      messages.error(request, 'Passwords do not match')
      return redirect('register')
  else:
    return render(request, 'accounts/register.html')

def login(request):
  if request.method == 'POST':
    username = request.POST['username']
    password = request.POST['password']

    user = auth.authenticate(username=username, password=password)

    if user is not None:
      auth.login(request, user)
      messages.success(request, 'You are now logged in')
      return redirect('dashboard')
    else:
      messages.error(request, 'Invalid credentials')
      return redirect('login')
  else:
    return render(request, 'accounts/login.html')

def logout(request):
  if request.method == 'POST':
    auth.logout(request)
    messages.success(request, 'You are now logged out')
    return redirect('index')

def dashboard(request):
  pending_contacts_made_to_receive_money = Contact.objects.order_by('-contact_date').filter(contactor_id=request.user.id)
  pending_contacts_to_send_money = Contact.objects.order_by('-contact_date').filter(contactee_id=request.user.id).filter(is_pending=True)
  context = {
    'pending_contacts_made_to_receive_money': pending_contacts_made_to_receive_money,
    'pending_contacts_to_send_money': pending_contacts_to_send_money
  }
  context['key'] = settings.STRIPE_PUBLISHABLE_KEY

  return render(request, 'accounts/dashboard.html', context)

@login_required
def edit(request):
  if request.method == 'POST':
    u_form = UserUpdateForm(request.POST, instance=request.user)
    if u_form.is_valid():
      u_form.save()
      messages.success(request, f'Your account has been updated')
      return redirect('dashboard')
  else:
    u_form = UserUpdateForm(instance=request.user)


  context = {
    'u_form': u_form
  }

  return render(request, 'accounts/edit.html', context)

def repayment(request):
  if request.method == 'POST':
    #create new payment
    title = request.POST['title']
    # recipient = request.POST.get('recipient_id')
    recipient = request.POST['recipient_id']
    payer = request.POST['payer_id']
    amount = request.POST.get('amount', 50)
    # amount = request.POST['amount']
    message = request.POST['message']
    payment_date = datetime.now()
    # receive_contact_id = request.POST['receive_contact_id']
    receive_contact_id = request.POST.get('receive_contact_id', 50)

    print('amt: ' + amount)
    # amount = int(float(amount))

    
    # x = User.objects.filter(pk=recipient)[0]
    # # listing = Payment(title=title, recipient=x, amount=50, message=message, payment_date=payment_date, payer_id=1)
    listing = Payment(title=title, recipient=recipient, payer=payer, amount=amount, message=message, payment_date=payment_date, request=receive_contact_id)
    listing.save()

    #update is_pending on request
    # Contact.objects.order_by('-contact_date').filter(contactor_id=request.user.id)
    Contact.objects.filter(pk=receive_contact_id).update(is_pending=False)

    messages.success(request, 'Your payment has been submitted')
    return redirect('dashboard')
    
class AccountListView(ListView):
  model = User
  template_name = 'accounts/all.html'
  context_object_name = 'accounts'
  
  def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['key'] = settings.STRIPE_PUBLISHABLE_KEY
        return context

def charge(request): 
    if request.method == 'POST':
        #create new payment
        title = request.POST['title']
        # recipient = request.POST.get('recipient_id')
        recipient = request.POST['recipient_id']
        payer = request.POST['payer_id']
        amount = request.POST.get('amount', 50)
        # amount = request.POST['amount']
        message = request.POST['message']
        payment_date = datetime.now()
        receive_contact_id = request.POST['receive_contact_id']
        receive_contact_id = 7
        print('amt: ' + amount)
        # amount = int(float(amount))

        
        # x = User.objects.filter(pk=recipient)[0]
        # # listing = Payment(title=title, recipient=x, amount=50, message=message, payment_date=payment_date, payer_id=1)
        listing = Payment(title=title, recipient=recipient, payer=payer, amount=amount, message=message, payment_date=payment_date, contact=receive_contact_id)
        listing.save()

        #update is_pending on request
        # Contact.objects.order_by('-contact_date').filter(contactor_id=request.user.id)
        Contact.objects.filter(pk=receive_contact_id).update(is_pending=False)

        messages.success(request, 'Your payment has been submitted')
        charge = stripe.Charge.create(
            amount=amount*100,
            currency='usd',
            description=title,
            source=request.POST['stripeToken']
        )
        return redirect('dashboard')