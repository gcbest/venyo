from django.test import SimpleTestCase
from django.urls import reverse, resolve
from accounts.views import dashboard, repayment, AccountListView

class TestURLs(SimpleTestCase):

    def test_dashboard_url_resolves(self):
        url = reverse('dashboard')
        print(resolve(url))
        self.assertEquals(resolve(url).func, dashboard)

    def test_account_list_view_url_resolves(self):
        url = reverse('all')
        print(resolve(url))
        self.assertEquals(resolve(url).func.view_class, AccountListView)