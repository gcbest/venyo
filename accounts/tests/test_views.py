from django.test import TestCase, Client
from django.urls import reverse
import json


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.dashboard_url = reverse('dashboard')

    def test_project_list_GET(self):
        response = self.client.get(self.dashboard_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/dashboard.html')

    