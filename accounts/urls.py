from django.urls import path
from .views import AccountListView
from . import views

urlpatterns = [
    path('login', views.login, name='login'),
    path('register', views.register, name='register'),
    path('logout', views.logout, name='logout'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('edit', views.edit, name='edit'),
    path('repayment', views.repayment, name='repayment'),
    path('all', AccountListView.as_view(), name='all')
]
