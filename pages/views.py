from django.shortcuts import render
from django.http import HttpResponse
from payments.choices import price_choices, bedroom_choices, state_choices

from payments.models import Payment

def index(request):
    return render(request, 'pages/index.html')

def login(request):
    return render(request, 'accounts/login.html')

def home(request):
	return render(request, 'pages/home.html')